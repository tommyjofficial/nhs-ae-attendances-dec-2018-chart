#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# In[2]:


df= pd.read_csv('Dec2019AE.csv')  # read csv file


# In[17]:


#calculating percentage
df['Percentage of attendances over 4hrs Type 1']= df['Number of attendances over 4hrs Type 1'] / df['Number of A&E attendances Type 1'] * 100
# top 10
df.sort_values(by=['Percentage of attendances over 4hrs Type 1'], ascending=False)[1:11]


# In[47]:


f = plt.figure()
f.set_figwidth(15)
f.set_figheight(10)

x=df.sort_values(by=['Percentage of attendances over 4hrs Type 1'], ascending=False)[1:11]['Org name']
y=df.sort_values(by=['Percentage of attendances over 4hrs Type 1'], ascending=False)[1:11]['Percentage of attendances over 4hrs Type 1']

plt.bar(x, y)
plt.xticks(rotation=90)
plt.ylabel(df.columns.values[-1], fontsize=14)
plt.xlabel('Name of organisation', fontsize=14)
plt.title('Top 10 NHS England organisations with Type 1 A&E attendances over 4hrs (%), Dec 2018', fontsize=18)
plt.figtext(.4, .85, "Source: NHS England A&E Attendances and Emergency Admissions 2018-19 dataset for December 2018")
plt.show()


# In[ ]:





# In[ ]:




